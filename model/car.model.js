const mongoose = require('mongoose')

const carSchema = mongoose.Schema({
    driver: mongoose.Types.ObjectId,
    name: String,
    year: Number,
    plate: String
})

module.exports = mongoose.model('Car', carSchema)