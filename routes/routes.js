const driverRoutes = require('./driver.routes')
const carRoutes = require('./car.routes')

module.exports = (app) => {
    driverRoutes(app)
    carRoutes(app)    
}