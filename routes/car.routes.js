const repository = require("../repository/car.repository")

module.exports = (app) => {

    app.get('/car/byDriver/:driverId', async (req, res) => {
        let driverId = req.params.driverId
        let result = await repository.findByDriver(driverId)
        res.json(result)
    })

    app.post('/car', async (req, res) => {
        await repository.save(req.body)
        res.end()
    })
}