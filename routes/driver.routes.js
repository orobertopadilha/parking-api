const repository = require("../repository/driver.repository")

module.exports = (app) => {

    app.get('/driver', async (req, res) => {
        let result = await repository.findAll()
        res.json(result)
    })

    app.post('/driver', async (req, res) => {
        await repository.save(req.body)
        res.end()
    })

    app.put('/driver', async (req, res) => {
        await repository.update(req.body)
        res.end()
    })

    app.delete('/driver/:id', async (req, res) => {
        let id = req.params.id
        await repository.delete(id)
        res.end()
    })
}