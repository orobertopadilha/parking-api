const Car = require('../model/car.model')
const database = require('../config/database')

exports.findByDriver = async (driverId) => {
    let con = await database.connect()
    let result = await Car.find({ driver: driverId})

    con.disconnect()
    return result
}

exports.save = async (data) => {
    let con = await database.connect()
    
    let car = new Car(data)
    await car.save()

    con.disconnect()
}