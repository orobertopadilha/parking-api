const Driver = require('../model/driver.model')
const database = require('../config/database')

exports.save = async (data) => {
    let con = await database.connect()
    
    let driver = new Driver(data)
    await driver.save()

    con.disconnect()
}

exports.findAll = async () => {
    let con = await database.connect()
    let result = await Driver.find()

    con.disconnect()
    return result
}

exports.update = async (driver) => {
    let con = await database.connect()
    await Driver.findByIdAndUpdate(driver._id, driver)
    con.disconnect()
}

exports.delete = async (driverId) => {
    let con = await database.connect()
    await Driver.findByIdAndDelete(driverId)
    con.disconnect()
}